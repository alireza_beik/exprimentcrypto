import websocket
import json
import requests
import threading


class Binance:
    def __init__(self):
        self.order_book = {}
        self.u = 0
        self.stream()

    def get_snapshot(self):
        url = "https://api.binance.com/api/v3/depth"
        params = {
            "symbol": "BTCUSDT",
            "limit": 1000
        }
        response = requests.get(url=url, params=params)
        order_book = response.json()
        bids = []
        for bid in order_book['bids']:
            c_bid = [float(bid[0]), float(bid[1])]
            bids.append(c_bid)

        asks = [[float(ask[0]), float(ask[1])] for ask in order_book['asks']]
        order_book['bids'] = bids
        order_book['asks'] = asks
        return order_book

    def stream(self):
        def on_close(ws):
            print("connection closed")

        def on_message(ws, msg):
            try:
                print(msg)
                data = json.loads(msg)
                if 'e' in data:
                    if data['e'] == 'depthUpdate':
                        if data['u'] <= self.order_book['lastUpdateId']:
                            print('do nothing')
                        elif data['U'] <= self.order_book['lastUpdateId'] + 1 <= data['u']:
                            print('first event')
                            self.update_order_book(data)
                            self.u = data['u']
                            pass
                        elif data['U'] == self.u + 1:
                            print('new event')
                            self.update_order_book(data)
                            self.u = data['u']
                        else:
                            self.order_book = self.get_snapshot()
                            print('else')
            except Exception as e:
                print(e)

        def on_open(ws):
            self.order_book = self.get_snapshot()
            print("connection opened")
            subscribe = {
                "method": "SUBSCRIBE",
                "params":
                    [
                        #"btcusdt@aggTrade",
                        "btcusdt@depth"
                    ],
                "id": 1
            }
            ws.send(json.dumps(subscribe))

        def on_error(ws, err):
            print(err)




        websocket.enableTrace(True)
        print("BINANCE OPENING")
        url = "wss://stream.binance.com:9443/ws/test"
        ws = websocket.WebSocketApp(url=url,
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close,
                                    on_open=on_open
                                    )
        thread = threading.Thread(target=ws.run_forever)
        thread.start()
        # ws.run_forever()
        print("BINANCE RUNING")

    def update_order_book(self, data):
        bids = {}
        for bid in self.order_book['bids']:
            bids[bid[0]] = bid[1]
        for b in data['b']:
            price = float(bid[0])
            volume = float(bid[1])
            bids[price] = volume
            if volume == 0:
                del bids[price]
        updated_price = sorted(bids, reverse=True)
        self.order_book['bids'] = [[price, bids[price]] for price in updated_price]

        asks = {}
        for ask in self.order_book['asks']:
            asks[ask[0]] = ask[1]
        for a in data['a']:
            price = float(a[0])
            volume = float(a[1])
            asks[price] = volume
            if volume == 0:
                del asks[price]
        updated_price = sorted(asks)
        self.order_book['asks'] = [[price, asks[price]] for price in updated_price]
        print('bid', self.order_book['bids'][0][0], 'ask', self.order_book['asks'][0][0])

b = Binance()
print('streamer started')
