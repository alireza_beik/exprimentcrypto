import websocket
import json
import requests


def get_snapshot():
    url = "https://api.binance.com/api/v3/depth"
    params = {
        "symbol": "BTCUSDT",
        "limit": 1000
    }
    response = requests.get(url=url, params=params)
    return response.json()


order_book = get_snapshot()



def on_message(ws, msg):
    try:
        print(msg)
        data = json.loads(msg)
        if 'e' in data:
            if data['e'] == 'depthUpdate':
                if data['u'] <= order_book['lastUpdateId']:
                    return None
                elif data['U'] <= order_book['lastUpdateId'] + 1 and data['u'] >= order_book['lastUpdateId'] + 1:
                    order_book['u'] = data['u']
                    pass
                elif 'u' in order_book and data['U'] == order_book['u'] + 1:
                    pass
    except Exception as e:
        print(e)



def on_open(ws):
    print("connection opened")
    subscribe = {
        "method": "SUBSCRIBE",
        "params":
            [
                #"btcusdt@aggTrade",
                "btcusdt@depth"
            ],
        "id": 1
    }
    ws.send(json.dumps(subscribe))


def on_close(ws):
    print("connection closed")


def on_error(ws, err):
    print(err)




websocket.enableTrace(True)
print("BINANCE OPENING")
url = "wss://stream.binance.com:9443/ws/test"
ws = websocket.WebSocketApp(url=url,
                            on_message=on_message,
                            on_error=on_error,
                            on_close=on_close,
                            on_open=on_open
                            )
ws.run_forever()
print("BINANCE RUNING")
