import requests

# get snapshot
url = 'https://www.binance.com/api/v3/depth?symbol=BTCUSDT&limit=1000'
response = requests.get(url)
order_book = response.json()
print(order_book)

import json
import websocket

#try:
 #   import thread
#except ImportError:
    #import _thread as thread
import time


def on_message(ws, message):
    data = json.loads(message)
    print(data)


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    ws.send(json.dumps({
        'method': 'SUBSCRIBE',
        'params': ['btcusdt@depth'],
        'id': 1
    }))

    # if __name__ == "__main__":
websocket.enableTrace(True)
print('BINANCE OPENING')
url = "wss://stream.binance.com:9443/ws/test"
ws = websocket.WebSocketApp(url=url,
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close,
                                on_open=on_open
                                )
ws.run_forever()
print('BINANCE RUNING')
